import Head from 'next/head';
import HeaderHome from '../components/HeaderHome';
import IntroduceProject from '../components/IntroduceProject';
import GeographicalLocation from '../components/GeographicalLocation';
import ViewDaiAn from '../components/ViewDaiAn';
import LuxHome from '../components/LuxHome';
import ReasonVinhomesHot from '../components/ReasonVinhomesHot';
import Subdivision from '../components/Subdivision';
import FixedComponent from '../components/FixedComponent';
import Footer from '../components/Footer';
import Location from '../components/Location';
import GroundVinhomes from '../components/GroundVinhomes';
import ImagesShow from '../components/ImagesShow';
import Snow from '../components/Snow';

function Home() {

  return (
    <div className='home-root'>
      <Head>
        <title>Vinhomes Đại An</title>
        <meta name="description" content="Vinhomes Đại An" />
        <link href="https://fonts.googleapis.com/css2?family=Inter" rel="stylesheet" />
        <link rel="shortcut icon" href="https://cdn.haitrieu.com/wp-content/uploads/2022/01/Icon-Vinhomes.png" type="image/x-icon"></link>
      </Head>

      <main className='main-roots' id="toTop" >
        <HeaderHome />
        <IntroduceProject />
        <LuxHome />
        <ImagesShow />
        <Subdivision />
        <ViewDaiAn />
        <Location />
        <GeographicalLocation />
        <GroundVinhomes />
        <ReasonVinhomesHot />
        <FixedComponent />
        <Snow />
      </main>
      <Footer />
    </div>
  )
}

export default Home;
