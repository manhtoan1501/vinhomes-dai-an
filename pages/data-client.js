import React from 'react';
import Head from 'next/head'
import { useState, useEffect } from 'react';
import { database } from '../firebaseConfig';
import { collection, getDocs } from 'firebase/firestore';
import { Grid } from '@material-ui/core';
import { FileCopy } from '@material-ui/icons';

const dbInstance = collection(database, 'notes');

function FromComponent() {
  const [datas, setDatas] = useState([]);

  const getDatas = () => {
    getDocs(dbInstance)
      .then((data) => {
        setDatas(data.docs.map((item) => {
          return { ...item.data(), id: item.id }
        }));
      })
  }

  useEffect(() => {
    getDatas();
  }, [])

  return (
    <div className='data-client-root'>
      <Head>
        <title>Vinhomes Đại An</title>
        <meta name="description" content="Vinhomes Đại An" />
        <link href="https://fonts.googleapis.com/css2?family=Inter" rel="stylesheet" />
        <link rel="shortcut icon" href="https://cdn.haitrieu.com/wp-content/uploads/2022/01/Icon-Vinhomes.png" type="image/x-icon"></link>
      </Head>
      <table>
        <tbody>
          {Array.isArray(datas) && datas.length ? datas.map((note, index) => {
            return (
              <tr key={index} className='item-inner'>
                <td>
                  {index + 1}
                </td>
                <td>
                  {note?.name}
                </td>
                <td>
                  <Grid container justifyContent='space-between'>
                    <Grid item xs={11}>
                      {note?.phone}
                    </Grid>
                    <Grid item xs={1}>
                      <FileCopy
                        onClick={() => navigator.clipboard.writeText(note?.phone)}
                        style={{ fontSize: 20, color: '#1976d2', cursor: 'pointer' }}
                      />
                    </Grid>
                  </Grid>
                </td>
                <td>
                  <Grid container justifyContent='space-between'>
                    <Grid item xs={11}>
                    {note?.email}
                    </Grid>
                    <Grid item xs={1}>
                      <FileCopy
                        onClick={() => navigator.clipboard.writeText(note?.email)}
                        style={{ fontSize: 20, color: '#1976d2', cursor: 'pointer' }}
                      />
                    </Grid>
                  </Grid>
                </td>
              </tr>
            );
          }) : <div className='not-data'>Không có dữ liệu</div>}
        </tbody>
      </table>
    </div>
  )
}

export default FromComponent;
