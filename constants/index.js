
const introduceConten = [
    'Sau khi hoàn tất quy hoạch dự án Vinhomes Dream City, tập đoàn Vingroup tiếp tục cho ra mắt dự án Khu đô thị sinh thái Xanh thứ 2 tại thành phố Hưng Yên mang tên Vinhomes Đại An.',
    'Dự án Vinhomes Đại An Hưng Yên có tổng diện tích quy hoạch lên đến 293,96ha với tổng mức đầu tư dự án khoảng 32.661 tỷ đồng (khoảng 1,4 tỷ USD), trong đó vốn chủ sở hữu chiếm 15% tổng vốn đầu tư, 85% còn lại là vốn vay và vốn huy động.',
    'Cũng như Vinhomes Dream City, dự án Vinhomes Đại An sở hữu một vị trí vô cùng đắc địa khi nằm sát cạnh cao tốc 5B Hà Nội – Hải Phòng. Có thể nói Hưng Yên thời gian sắp tới chắc chắn sẽ trở thành tâm điểm thu hút mọi sự quan tâm với hai siêu dự án này.',
];

const contentLuxeHome = [
    'Tên dự án: Vinhomes Đại An (dự kiến)',
    'Chủ đầu tư: Tập đoàn Vingroup.',
    'Vị trí dự án: Thuộc địa phận 2 xã Tân Quang, thị trấn Như Quỳnh, huyện Văn Lâm và xã Nghĩa Trụ, huyện Văn Giang, tỉnh Hưng Yên.',
    'Quy mô dự án: 293,96 ha.',
    'Vốn đầu tư: 1,4 tỷ USD (32.661 tỷ đồng)',
    'Tiến độ thực hiện trong 6 năm kể từ 31/3/2021',
    'Đơn vị quản lý & vận hành: Vinhomes',
    'Loại hình sản phẩm: Căn hộ chung cư, biệt thự, liền kề, shophouse',
];

const utilities = [
    'Là một trong những dự án có quy mô đẳng cấp nhất thuộc chủ đầu tư Vingroup, Vinhomes Đại An chắc chắn sẽ được thừa hưởng những dịch vụ tiện ích đẳng cấp 5 sao mang thương hiệu Vinhomes nổi tiếng từ lâu. Ngoài ra, để đáp ứng tối đa nhu cầu của quý cư dân và hướng đến mục tiêu Khu đô thị sinh thái Xảnh đẳng cấp Quốc Tế, Vinhomes Đại An còn được CĐT quy hoạch hàng loạt những tiện ích công cộng, vui chơi, giải trí…. cụ thể như:',
    'Phòng khám quốc tế Vinmec. Trường mầm non & tiểu học Vinschool. Siêu thị Vinmart. Tổ hợp thể thao: sân tennis, sân bóng rổ, cầu lông,…Hệ thống những khu vườn độc đáo Hệ thống dịch vụ cư dân tiêu chuẩn 5 sao. Hệ thống đường dạo bộ. Tiện nghi sống tiên tiến: hầm để xe thông minh, máy phát điện dự phòng 100%',
    'Vinhome Đại An được nghiên cứu, đầu tư bài bản, chăm chút đến từng chi tiết để kiến tạo nên một không gian sống hoàn hảo. Tương lai hai khu đô thị lớn tại Hưng Yên này sẽ sớm trở thành một biểu tượng mới khi nhắc tới Vinhomes. Tại dự án có đầy đủ hết các dịch vụ cùng hệ thống tiện ích All in one trứ danh của Vinhomes.',
];

const groundVinhomes = [
    'Khu Đô thị Đại An được quy hoạch định hướng phát triển trên cơ sở tối ưu tối đa những thuận lợi từ kết nối giao thông tại khu vực. Các khu vực trong khu đô thị Vinhomes Đại An được xây dựng trên cơ sở tổ chức mạng lưới ô bàn cờ, kết hợp hài hòa đồng bộ giữ giao thông đối nội và đối ngoại.',
    'Tổng thể mặt bằng dự án Vinhomes Đại An được chia thành 4 phân khu.',
    'Với trục đường chính có mặt cắt 30m – 40m, phân chia khu vực thành các khu nhà ở mang nét đặc trưng khác nhau. Mỗi nhóm nhà được kết nối với trục đường chính bằng mạng lưới đường nội bộ rộng 13m-15m, đan xen hài hoà.',
];

const listGround = [
    'Khu nhà cao tầng phía Tây Nam',
    'Khu nhà ở biệt thự phía Tây Bắc',
    'Khu nhà ở thấp tầng phía Đông Bắc',
    'Khu nhà ở phía Đông Nam với khu vui chơi giải trí, khu thương mại, dịch vụ, nhà ở thấp tầng',
];

const images = [
    {
      original: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3464216634352_09d31325794799fc58ed0175d056cf4c-20220603154246.jpg',
      thumbnail: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3464216634352_09d31325794799fc58ed0175d056cf4c-20220603154246.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3464216629420_23d4604f61e43a671db2300944ff2f58-20220603154221.jpg',
      thumbnail: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3464216629420_23d4604f61e43a671db2300944ff2f58-20220603154221.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/279093731_130386709576960_6076406005806999168_n-20220603153702.jpg',
      thumbnail: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/279093731_130386709576960_6076406005806999168_n-20220603153702.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/co-xanh_1652088039-20220603104631.png',
      thumbnail: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/co-xanh_1652088039-20220603104631.png',
    },
    {
      original: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3465605220599_f4ac200d76b404b128d2c5c1e4e85ed9-20220603160137.jpg',
      thumbnail: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3465605220599_f4ac200d76b404b128d2c5c1e4e85ed9-20220603160137.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/sao-bien_1652088040-20220603104631.png',
      thumbnail: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/sao-bien_1652088040-20220603104631.png',
    },
    {
      original: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/san-ho_1652088040-20220603104631.png',
      thumbnail: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/san-ho_1652088040-20220603104631.png',
    },
    {
      original: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/hai-au_1652088039-20220603104631.png',
      thumbnail: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/hai-au_1652088039-20220603104631.png',
    },
    {
      original: 'https://w.ladicdn.com/s700x550/5f29630391d6475c7048ef05/shophouse-vinhomes-the-empire-2-20220423104417.jpg',
      thumbnail: 'https://w.ladicdn.com/s700x550/5f29630391d6475c7048ef05/shophouse-vinhomes-the-empire-2-20220423104417.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/viber_image_2022-04-24_11-55-48-557-20220603104637.jpg',
      thumbnail: 'https://w.ladicdn.com/s450x400/5fe010fc282929001288f3c2/viber_image_2022-04-24_11-55-48-557-20220603104637.jpg',
    },
    {
      original: 'https://w.ladicdn.com/s400x400/5a4c72d8354e7f4f9f1bc22b/285447141_141633705118927_2027664869940039963_n-20220609172330.jpg',
      thumbnail: 'https://w.ladicdn.com/s400x400/5a4c72d8354e7f4f9f1bc22b/285447141_141633705118927_2027664869940039963_n-20220609172330.jpg',
    }
  ];

export {
    introduceConten,
    contentLuxeHome,
    utilities,
    groundVinhomes,
    listGround,
    images
}