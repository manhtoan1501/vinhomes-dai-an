import { Grid } from '@material-ui/core';


const ViewHoaBinh = () => {

  const listImage = [
    { image: 'https://vinhomes-daian.com/wp-content/uploads/2021/09/biet-thu-vinhomes-dai-an-2.jpg' },
    { image: 'https://w.ladicdn.com/s800x600/5a4c72d8354e7f4f9f1bc22b/z3464216629420_23d4604f61e43a671db2300944ff2f58-20220603154221.jpg' },
    { image: 'https://vinhomes-daian.com/wp-content/uploads/2021/09/nha-pho-vinhomes-dai-an.jpg' },
    { image: 'https://vinhomes-daian.com/wp-content/uploads/2021/09/chung-cu-vinhomes-dai-an-1.jpg' },
    { image: 'https://vinhomes-daian.com/wp-content/uploads/2021/09/vinhomes-dai-an-test-1.jpg' },
    { image: 'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhome-dai-an-hung-yen.jpg' },
    { image: 'https://w.ladicdn.com/s700x550/5fe010fc282929001288f3c2/z3472617781739_30a831d5373e9713aeaba63cb38864a2-20220607032420.jpg' },
    { image: 'https://w.ladicdn.com/5fe010fc282929001288f3c2/canh-quan-phan-khu-co-xanh-vinhomes-the-empire-20220719064553.png' },
    { image: 'https://w.ladicdn.com/s1000x800/5fe010fc282929001288f3c2/hai-au_1652088039-20220603104631.png' },
  ];

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          SẢN PHẨM TẠI VINHOMES ĐẠI AN          
        </h1>
        <p className='content-base'>
          <strong>Vinhomes Đại An</strong> là khu đô thị All in one với đầy đủ các loại hình sản phẩm cũng như tiện ích đồng bộ để phục vụ mọi nhu cầu của cư dân và mang tới khách hàng những trải nghiệm sống tuyệt vời nhất. Do đó không thể thiếu được các dòng sản phẩm tại <strong>Vinhomes Đại An</strong> như: Biệt thự, liền kề, nhà phố và chung cư. Nếu như trước đây việc sở hữu một căn Biệt thự Vinhomes luôn là điều mà nhiều khách hàng mong muốn nhưng khó có được. Nhưng giờ đây với số lượng thấp tầng nhiều tại Dự án <strong>Vinhomes Đại An</strong> thì giấc mơ sở hữu một căn Biệt thự liền kề Vinhomes là một điều không còn quá xa vời nữa.
        </p>
      </Grid>
      {listImage.map((element, index) => (
        <Grid xs={12} sm={6} md={4} item key={index}>
          <img src={element.image} className='image-view-location' />
        </Grid>
      ))}
    </Grid>
  )
}

export default ViewHoaBinh;
