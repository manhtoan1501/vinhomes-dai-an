
import { Grid } from '@material-ui/core';
import { CheckCircle } from '@material-ui/icons';
import { contentLuxeHome } from  '../constants';


const LuxHome = () => {

  return (
    <Grid container className='block-location max-width-base' alignItems='flex-start' spacing={2}>
      <Grid item xs={12} md={6}>
        <h1 className='title-base'>
          SƠ LƯỢC VỀ VINHOMES ĐẠI AN
        </h1>
        {contentLuxeHome.map((element, index) => (
          <p className='content-text-base position-center-width' key={index}>
            <CheckCircle style={{ paddingRight: 8, fontSize: 28, color: '#2979ff' }} /> {element}
          </p>
        ))}
      </Grid>
      <Grid item xs={12} md={6}>
        <img
          src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhome-dai-an-hung-yen.jpg'}
          className='image-luxe-home'
        />
      </Grid>
    </Grid>
  );
}

export default LuxHome;
