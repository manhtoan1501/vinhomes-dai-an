import { Grid } from '@material-ui/core';
import { introduceConten } from '../constants';

const Location = () => {

  return (
    <Grid
      container
      alignItems='center'
      justifyContent='space-between'
      spacing={2}
      className='location-root'
    > 
      <Grid item xs={12} className='location-body'>
        <div className='body-content'>
          <h1 className='title-base'>
            VỊ TRÍ VINHOMES ĐẠI AN
          </h1>
          <p className='content'>
            Dự án Vinhomes Đại An nằm ở địa phận 2 xã là Tân Quang – huyện Văn Lâm và xã Nghĩa Trụ – huyện Văn Giang, tỉnh Hưng Yên. Đây là cửa ngõ phía Tây Bắc tỉnh Hưng Yên, cách thành phố Hưng Yên 37 km về phía Tây Bắc. Dự án Vinhomes Đại An nằm rât gần 3 Khu đô thị lớn là Vinhomes Ocean Park Gia Lâm, Vinhomes Dream City, Ecopark Hưng Yên
          </p>
          <p className='content'>
            Nhờ việc nằm tại cửa ngõ phía Tây Bắc của thành phố Hưng Yên, Vinhomes Đại An rất được chú trọng đầu tư và phát triển hệ thống giao thông , hạ tầng đồng bộ, thuận tiện cho việc di chuyển và kết nối muôn nơi.
          </p>
          {/* <img
            src="https://vinhomesdaian.vn/wp-content/uploads/2021/06/vi-tri-vinhomes-dai-an-hung-yen.jpg"
            className="image-location-dai-an"
            alt="vị trí vinhomes đại an hưng yên"
            loading="lazy"
          /> */}
        </div>
      </Grid>
    </Grid>
  );
}

export default Location;
