import React from 'react';
import Link from 'next/link'

const ImageHome= () => {
   
    const headers = [
        {
            title: 'Trang chủ',
            link: '/',
        },
        // {
        //     title: 'Vị trí',
        //     link: '/vi-tri',
        // },
        // {
        //     title: 'Tin tức',
        //     link: '/tin-tuc',
        // },
    ];

    return (
        <div className='header-home-root'>
            {/* <div className='header-container'>
                {headers.map((element, index) => (
                    <Link
                        key={index}
                        href={element.link}
                    >
                        <a>
                            {element.title}
                        </a>
                    </Link>
                ))}
            </div> */}
            <img
                // src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhomes.jpg'}
                src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhome-dai-an-hung-yen-3.jpg'}
                className='image-home'
            />
        </div>
    );
}

export default ImageHome;