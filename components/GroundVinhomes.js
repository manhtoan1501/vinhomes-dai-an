import { Grid } from '@material-ui/core';
import {
  groundVinhomes,
  listGround,
} from '../constants';


const GeographicalLocation = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          MẶT BẰNG VINHOMES ĐẠI AN
        </h1>
        {groundVinhomes.map((element, index) => (
          <p className='content-base' key={index}>
            {element}
          </p>
        ))}
        <ul>
          {listGround.map((element, index) => (
            <li className='center-base'>
              {element}
            </li>
          ))}
        </ul>
      </Grid>
      <Grid item md={6} xs={12}>
        <img
          src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhomes-dai-an-hung-yen-2.jpg'}
          className='image-ground-location'
        />
      </Grid>
      <Grid item md={6} xs={12}>
        <img
          src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhomes-dai-an-hung-yen-3.jpg'}
          className='image-ground-location'
        />
      </Grid>
    </Grid>
  );
}

export default GeographicalLocation;
