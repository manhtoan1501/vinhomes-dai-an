import { Grid } from '@material-ui/core';


const ReasonVinhomesHot = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          CHỦ ĐẦU TƯ DỰ ÁN VINHOMES ĐẠI AN TẦM CỠ NHƯ THẾ NÀO?
        </h1>
        <p className='content-base'>
          Nói đến Chủ đầu tư các Dự án của Việt Nam hiện nay không thể nhắc đến tên tuổi của Vinhomes. Là Chủ đầu tư top 1, uy tín hàng đầu trong lĩnh vực bất động sản từ Bắc vào Nam với hàng loạt các Dự án bất động sản nổi tiếng mang thương hiệu Vinhomes. <strong>Vinhomes Đại An</strong> là một sản phẩm cao cấp được quy hoạch bài bản của Vinhomes, nằm ở vị trí đắc địa với rất nhiều lợi thế.
        </p>
        <p className='content-base'>
          Công ty Cổ phần Vinhomes là công ty đầu tư, kinh doanh và quản lý bất động sản số 1 Việt Nam. Hiện nay Vinhomes có quy mô, tốc độ phát triển và chất lượng dịch vụ đẳng cấp, dẫn dắt thị trường bất động sản Việt Nam phát triển bền vững, tiến tới vươn tầm quốc tế.
        </p>
        <img
          src="http://vinhomes-daian.com/wp-content/uploads/2021/09/top-10-cong-ty-2021.jpg"
          className='image-reasson-vinhomes'
        />
      </Grid>
    </Grid>
  );
}

export default ReasonVinhomesHot;
