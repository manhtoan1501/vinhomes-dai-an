import { Grid } from '@material-ui/core';
import { introduceConten } from '../constants';
const Advertise = () => {

  return (
    <Grid
      container
      alignItems='center'
      justifyContent='space-between'
      spacing={2}
      className='block-location max-width-base'
    >
      <Grid item xs={12}>
        <h1 className='title-base'>
          VINHOMES ĐẠI AN HƯNG YÊN
        </h1>
        {introduceConten.map((element, index) => (
          <p className='content-base' key={index}>
            {element}
          </p>
        ))}
      </Grid>
    </Grid>
  );
}

export default Advertise;
