import ImageGallery from 'react-image-gallery';
import { Grid } from '@material-ui/core';
import { images } from '../constants';


const MyGallery = () => {
  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <ImageGallery items={images} />
      </Grid>
    </Grid>
  );
}

export default MyGallery;
