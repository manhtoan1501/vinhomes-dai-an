import { Grid } from '@material-ui/core';


const Subdivision = () => {

  const subdivisions = [
    {
      title: 'Biệt Thự',
      image: 'https://vinhomesdaian.vn/wp-content/uploads/2021/06/biet-thu-vinhomes-dai-an-800x500.jpg',
      content: 'Là sản phẩm cao cấp nhất tại dự án Vinhomes Đại An Hưng Yên, với các loại hình biệt thự như: biệt thự đơn lập, biệt thự song lập, biệt thự tứ lập. Sở hữu các vị trí đắc địa tại các khu vực trung tâm với hệ thống cảnh quan cây xanh,…',
    },
    {
      title: 'Liền Kề',
      image: 'https://vinhomesdaian.vn/wp-content/uploads/2021/06/lien-ke-vinhomes-dai-an-1.jpg',
      content: 'Liền kề Vinhomes Đại An là sản phẩm có mức giá tốt nhất trong các sản phẩm thấp tầng, được thiết kế 4 tầng sang trọng với 4 – 6 phòng ngủ và 1 khoảng sân vườn nhỏ trước nhà giúp chủ nhân có thể tuỳ ý trồng cây xanh theo sở thích….',
    },
    {
      title: 'Shophouse',
      image: 'https://vinhomesdaian.vn/wp-content/uploads/2021/06/shophouse-vinhomes-dai-an-800x500.jpg',
      content: 'Nằm tại các vị trí mặt đường lớn bên trong dự án, Shophouse Vinhomes Đại An chắc chắn sẽ  là sản phẩm được các nhà đầu tư chú ý bởi khả năng vừa ở vừa kinh doanh ưu việt, tiềm năng kinh doanh sinh lời cao…',
    },
    {
      title: 'Chung Cư',
      image: 'https://vinhomesdaian.vn/wp-content/uploads/2021/06/vinhome-dai-an-hung-yen-1-767x500.jpg',
      content: 'Các căn hộ chung cư Vinhomes Đại An được thiết kế bởi các tập đoàn thiết kế hàng đầu thế giới, mang đến không gian sống sang trọng, hiện đại, như được sống tại Resort 5 sao, cùng với hệ thống dịch vụ tiện ích đẳng cấp…',
    }
  ];
  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          CÁC LOẠI HÌNH SẢN PHẨM
        </h1>
      </Grid>
      {subdivisions.map((element, index) => (
        <Grid item xs={12} sm={6} md={3} key={index}>
          <div className='subdivision-item-root'>
            <img
              src={element.image}
              className='image-subdivision'
            />
            <p className='title-subdivision'>
              {element.title}
            </p>
            <p className='content-subdivision'>
              {element.content}
            </p>
          </div>
        </Grid>
      ))}
    </Grid>
  );
}

export default Subdivision;
