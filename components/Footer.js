import { Grid, Button } from '@material-ui/core';
import Form from './Form';


const Footer = () => {

  const promises = [
    'Đội ngũ chuyên viên tư vấn chuyên nghiệp, trung thực, nhiệt tình',
    'Mọi thủ tục pháp lý khách hàng đều được thực hiện trực tiếp với CĐT',
    'Cung cấp thông tin nhanh chóng, chính xác và nhanh nhất từ chủ đầu tư',
    'Cam kết bán hàng trực tiếp giá CĐT'
  ];

  const projects = [
    'Vinhomes Riverside',
    'Vinhomes Royal City',
    'Vinhomes Ocean Park',
    'Vinpearl Phú Quốc',
    'Vinhomes Grand Park',
    'Vinhomes Time City'
  ];

  const inputs = [
    {
      name: 'NAME',
      placeholder: 'nhập tên'
    },
    {
      name: 'EMAIL',
      placeholder: 'nhập email'
    },
    {
      name: 'SDT',
      placeholder: 'nhập số điện thoại'
    }
  ];

  return (
    <Grid container className='footer-root'>
      <Grid item xs={12} md={4}>
        <div className='item-footer'>
          <p className='title-footer'>
            THÔNG TIN DỰ ÁN
          </p>
          <p className='item-content'>
            <strong>Tên:</strong> Vinhomes The Empire (Vinhomes Dream City)
          </p>
          <p className='item-content'>
            <strong>Chủ đầu tư:</strong> Vingroup
          </p>
          <p className='item-content'>
            <strong>Vị trí:</strong> Long Hưng & Nghĩa Trụ – Huyện Văn Giang – Hưng Yên
          </p>
          <p className='item-content'>
            <strong>Giới thiệu:</strong> Vinhomes The Empire (Dream City) là siêu dự án khu đô thị sinh thái đẳng cấp tại thành phố Hưng Yên.
          </p>
          <p className='item-content'>
            <strong>Hotline:</strong> 0979 194 717
          </p>
        </div>
      </Grid>
      <Grid item xs={12} md={4}>
        <div className='item-footer'>
          <p className='title-footer'>
            ĐĂNG KÝ NHẬN BẢNG GIÁ
          </p>
          <Form />
        </div>
      </Grid>
      <Grid item xs={12} md={4}>
        <div className='item-footer'>
          <p className='title-footer'>
            DỰ ÁN TIÊU BIỂU
          </p>
          {projects.map((element, index) => (
            <p className='item-content' key={index}>
              - {element}
            </p>
          ))}
        </div>
      </Grid>
      
    </Grid>
  )
}

export default Footer;
