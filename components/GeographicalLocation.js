import { Grid } from '@material-ui/core';
import { utilities } from '../constants';

const GeographicalLocation = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          TIỆN ÍCH VINHOMES ĐẠI AN
        </h1>
        {utilities.map((element, index) => (
          <p className='content-base' key={index}>
            {element}
          </p>
        ))}
      </Grid>
      <Grid item xs={12}>
        <img
          src={'https://vinhomesdaian.vn/wp-content/uploads/2021/06/tien-ich-vinhomes-dai-an.jpg'}
          className='image-location'
        />
      </Grid>
    </Grid>
  );
}

export default GeographicalLocation;
